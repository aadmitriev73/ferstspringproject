
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admins list</title>
</head>
<body>
${adminSecure}
<b><br>ADMINS List:<br></b>
<OP>
    <UL>Admin 1.</UL>
    <UL>Admin 2.</UL>
    <UL>Admin 3.</UL>
</OP>
<authorize access="isAuthenticated()">
    <br><a href="/index">Logout</a>
</authorize>
</body>
</html>
