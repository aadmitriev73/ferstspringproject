<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome.</title>
</head>
<body>
<h1>${index}</h1>
<br><a href="/userSecure">Page for users.</a>
<br><a href="/adminSecure">Page for admins.</a>
<authorize access="isAuthenticated()">
    <br><a href="/index">Logout</a>
</authorize>
</body>
</html>
