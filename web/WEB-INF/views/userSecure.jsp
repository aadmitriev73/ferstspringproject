<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>USERS List</title>
</head>
<body>
${userSecure}
<b><br>USERS List:<br></b>
<OP>
    <UL>User 1.</UL>
    <UL>User 2.</UL>
    <UL>User 3.</UL>
</OP>
<authorize access="isAuthenticated()">
    <br><a href="/index">Logout</a>
</authorize>
</body>
</html>
