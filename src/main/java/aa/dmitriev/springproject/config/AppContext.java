package aa.dmitriev.springproject.config;

import aa.dmitriev.springproject.model.AdminSecureAccess;
import aa.dmitriev.springproject.model.UserSecureAccess;
import aa.dmitriev.springproject.model.Welcome;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppContext {
    @Bean
    public Welcome welcome(){
        return new Welcome();
    }
    @Bean
    public UserSecureAccess userSecureAccess(){
        return new UserSecureAccess();
    }
    @Bean
    public AdminSecureAccess adminSecureAccess(){
        return new AdminSecureAccess();
    }
}