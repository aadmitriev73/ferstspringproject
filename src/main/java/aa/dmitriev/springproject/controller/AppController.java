package aa.dmitriev.springproject.controller;

import aa.dmitriev.springproject.model.AdminSecureAccess;
import aa.dmitriev.springproject.model.UserSecureAccess;
import aa.dmitriev.springproject.model.Welcome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AppController {
    @Autowired
    Welcome welcome;
    @Autowired
    UserSecureAccess userSecureAccess;
    @Autowired
    AdminSecureAccess adminSecureAccess;

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("index", welcome.getText());
        return "index";
    }

    @RequestMapping("/userSecure")
    public String secure(Model model) {
        model.addAttribute("userSecure", userSecureAccess.info());
        return "userSecure";
    }

    @RequestMapping("/adminSecure")
    public String superSecure(Model model) {
        model.addAttribute("adminSecure", adminSecureAccess.info());
        return "adminSecure";
    }
}