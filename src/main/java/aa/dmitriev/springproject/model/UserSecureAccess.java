package aa.dmitriev.springproject.model;

public class UserSecureAccess {
    public String info(){
        return "Access is allowed only to registered users.";
    }
}